#mobile menu

This method is intended to use without jquery - it is very lightweight!

Call it:

<code>
mobileMenu(toClone,prependTo, params);
</code>

If there are more than one nav element (search,language), "toClone" can be a comma separated list of selectors.  
Ids (#) automatically will be rewritten (prefixed) and in case of form elements the for attribute adjusted as well

###params
	prefix: "mm-",  
	containerClass: "mm-container",   
	hiddenClass: "hidden,  
	mobileMenuClass: "mobile-menu",  
	mobileHeaderClass: "mobile-header",  
	revealMobileMenuClass: "reveal-mobile-menu",  
	toggleOnClass: "on",  
	hideSubMenus: false,  
	subMenuToggleButtonClass: "toggle",  
	wrapClass: "mm-wrap-",
	customHeaderContent: ""

